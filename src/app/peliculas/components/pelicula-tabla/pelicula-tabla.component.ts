import { Component, OnInit, Input } from '@angular/core';

import { Pelicula as IPelicula } from '../../../core/models/peliculas/pelicula.models';
import { PeliculasService } from '../../../core/services/peliculas/peliculas.service';

/**
 * @title Basic use of `<table mat-table>`
 */
@Component({
  selector: 'app-pelicula-tabla',
  templateUrl: './pelicula-tabla.component.html',
  styleUrls: ['./pelicula-tabla.component.scss']
})
export class PeliculaTablaComponent implements OnInit {

  @Input() peliculas: IPelicula[];
  starts = [];

  constructor(
    private peliculasService:PeliculasService
  ) { }

  ngOnInit(): void {
    this.peliculas = this.peliculasService.getPeliculas();
    //this.fetchPeliculas()
  }

  eliminar(id:number){
    return this.peliculasService.delete(id);

    /* Método con endpoint */

    /*return this.peliculasService.delete(id)
      .subscribe(pelicula => {
        console.log(pelicula)
      })*/
  }


  fetchPeliculas(){
    this.peliculasService.getPeliculasService()
      .subscribe(peliculas =>{
        console.log(peliculas)
        this.peliculas = peliculas
      })
  };
}
