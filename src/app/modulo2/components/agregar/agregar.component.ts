import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { Pelicula as IPelicula } from '../../../core/models/peliculas/pelicula.models';
import { PeliculasService } from '../../../core/services/peliculas/peliculas.service';
import { Router,ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.scss']
})
export class AgregarComponent implements OnInit{
  @Input()

  addressForm = this.fb.group({
    titulo: [null, Validators.required],
    pais: [null, Validators.required],
    clasificacion: [null, Validators.compose([
      Validators.required, Validators.minLength(1), Validators.maxLength(2)])
    ],
  });

  estrellas=[1,2,3,4,5,6,7,8,9,10];
  elementoSeleccionado:number;
  habilitarBtnCrear:Boolean = true;
  habilitarBtnEditar:Boolean = true;
  pelicula:IPelicula;

  constructor(
    private fb: FormBuilder,
    private peliculasService:PeliculasService,
    private router: Router,
    private route: ActivatedRoute,
  ) {}



  ngOnInit(): void {
    if (this.ruta[2] === 'update') {
      this.route.params.subscribe((params: Params) =>{
        const id = params.id
        console.log(`El titulo de la pelicula es ${id}`)
        this.pelicula = this.peliculasService.getPeliculaDetalle(id)
        //this.fetchPelicula(id)
        console.log(this.pelicula)
      })

      this.addressForm = this.fb.group({
        titulo: [this.pelicula.titulo, Validators.required],
        pais: [this.pelicula.pais, Validators.required],
        clasificacion: [Number(this.pelicula.clasificacion), Validators.compose([
          Validators.required, Validators.minLength(1), Validators.maxLength(2)])
        ],
      });

    }
  }

  onSubmit(): void {
    if (this.ruta[2] === 'create') {
      this.crearPelicula()
    }
    if (this.ruta[2] === 'update') {
      this.actualizar()
    }
  }


  ruta=(this.router.url).split('/')

  crearPelicula(){
      this.habilitarBtnCrear = false
      const idPelicula = this.peliculasService.peliculas.length +1
      const clasif = Math.random() * (11 - 1) + 1
      /*const nuevaPelicula: IPelicula = {
        id: idPelicula,
        titulo: `Pelicula ${idPelicula}`,
        clasificacion: Number(clasif.toFixed()),
        pais: 'EEUU'
      }*/
      console.log('clasificación:'+this.elementoSeleccionado)
      const nuevaPelicula: IPelicula = {
        id: idPelicula,
        titulo: this.addressForm.value.titulo,
        clasificacion: this.elementoSeleccionado,
        pais: this.addressForm.value.pais
      }
      this.peliculasService.create(nuevaPelicula);
      setTimeout(() => {
        this.router.navigateByUrl('/movie');
      }, 2000);
    }

  actualizar(){
    this.habilitarBtnEditar = false

    const changePelicula: IPelicula = {
      id: this.pelicula.id,
      titulo: this.addressForm.value.titulo,
      clasificacion: this.elementoSeleccionado,
      pais: this.addressForm.value.pais
    }
    this.peliculasService.update(this.pelicula.id, changePelicula);

    setTimeout(() => {
      this.router.navigateByUrl('/movie');
    }, 2000);
  }

  /* Método de actualizacion con endpoint */
  actualizarEndpoint(){
    this.habilitarBtnEditar = false

    const cambiosPelicula: Partial<IPelicula> = {
      id: this.pelicula.id,
      titulo: this.addressForm.value.titulo,
      clasificacion: this.elementoSeleccionado,
      pais: this.addressForm.value.pais
    }
    this.peliculasService.updateEndpoint(this.pelicula.id, cambiosPelicula);

    setTimeout(() => {
      this.router.navigateByUrl('/movie');
    }, 2000);

  }
}
