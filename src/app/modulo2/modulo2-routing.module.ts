import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TopComponent } from './components/top/top.component';
import { AgregarComponent } from './components/agregar/agregar.component';

const routes: Routes = [
  {
    path:'top',
    component: TopComponent
  },
  {
    path:'create',
    component: AgregarComponent
  },
  {
    path:'update/:id',
    component: AgregarComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Modulo2RoutingModule { }
