import { Component, OnInit } from '@angular/core';

import { Pelicula as IPelicula} from '../../../core/models/peliculas/pelicula.models';
import { PeliculasService } from '../../../core/services/peliculas/peliculas.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


  listaPeliculas: IPelicula[]=[]

  constructor( private peliculasService:PeliculasService) { }

  ngOnInit(): void {
    let lista = this.peliculasService.getPeliculas();
    console.log(lista)
    this.listaPeliculas = lista
  }

}
