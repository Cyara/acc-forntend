import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//components
import { PeliculaComponent } from './components/pelicula/pelicula.component';
import { PeliculaDetalleComponent } from './components/pelicula-detalle/pelicula-detalle.component';
import { PeliculasComponent } from './components/peliculas/peliculas.component';
import { PeliculaTablaComponent } from './components/pelicula-tabla/pelicula-tabla.component';

// Modulos compartidos
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../material/material.module';

//Routing
import { PeliculasRoutingModule } from './peliculas-routing.module';

@NgModule({
  declarations: [
    PeliculasComponent,
    PeliculaComponent,
    PeliculaDetalleComponent,
    PeliculaTablaComponent,
  ],
  imports: [
    CommonModule,
    PeliculasRoutingModule,
    SharedModule,
    MaterialModule,
  ],
  providers: [],
})
export class PeliculasModule { }
