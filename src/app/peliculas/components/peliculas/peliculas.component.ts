import { Component, OnInit } from '@angular/core';

import { Pelicula as IPelicula} from '../../../core/models/peliculas/pelicula.models';
import { PeliculasService } from '../../../core/services/peliculas/peliculas.service';

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.scss']
})
export class PeliculasComponent implements OnInit {

  listaPeliculas: IPelicula[]=[]

  constructor( private peliculasService:PeliculasService) { }

  ngOnInit(): void {
    let lista = this.peliculasService.getPeliculas();
    console.log(lista)
    this.listaPeliculas = lista
    //this.fetchPeliculas()
  }

  /*clickPelicula(titulo:string){
    console.log('Prueba - titulo pelicula: '+titulo)
  }*/

  /*fetchPeliculas(){
    this.peliculasService.getPeliculas()
      .subscribe( pelicula => {
        console.log(pelicula)
        this.listaPeliculas = pelicula
      })
  }*/

}
