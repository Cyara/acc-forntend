import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PeliculasComponent } from './components/peliculas/peliculas.component';
import { PeliculaTablaComponent } from './components/pelicula-tabla/pelicula-tabla.component';
import { PeliculaDetalleComponent } from './components/pelicula-detalle/pelicula-detalle.component';

const routes: Routes = [
  {
    path:'',
    component:PeliculaTablaComponent,
  },
  {
    path:'create',
    component:PeliculaDetalleComponent,
  },
  {
    path:':id',
    component:PeliculaDetalleComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class PeliculasRoutingModule { }
