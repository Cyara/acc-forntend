import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
//model
import { Pelicula as IPelicula} from '../../../core/models/peliculas/pelicula.models';
//
import { PeliculasService } from '../../../core/services/peliculas/peliculas.service';

@Component({
  selector: 'app-pelicula',
  templateUrl: './pelicula.component.html',
  styleUrls: ['./pelicula.component.scss']
})
export class PeliculaComponent implements OnInit {

  @Input() pelicula: IPelicula;
  @Output() emmitShowDetail: EventEmitter<any> = new EventEmitter();

  starts = [];

  constructor(
    private peliculasService:PeliculasService,
  ) { }

  ngOnInit(): void {
    for (let index = 0; index < this.pelicula.clasificacion; index++) {
      this.starts.push(index)
    }
  }

}
